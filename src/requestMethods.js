import axios from "axios"

const BASE_URL = "https://floating-dusk-30212.herokuapp.com/api/" 
const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOWYyNWRkN2ExYjNhYTA3ODBjMmJhMiIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1NDYwMTk2NCwiZXhwIjoxNjU0ODYxMTY0fQ.ZJKcPPl3ueN1zDRR9sgRIUHET1kjnQanuCqAXtWjR5w" 

export const publicRequest = axios.create({
    baseURL : BASE_URL
})
export const userRequest = axios.create({
    baseURL : BASE_URL,
    header : {token: `Bearer ${TOKEN}`}
})


//  http://localhost:5000/api/

// https://floating-dusk-30212.herokuapp.com/api/

