import { ArrowLeftOutlined, ArrowRightOutlined } from '@material-ui/icons'
import {React, useState} from 'react'
import styled from 'styled-components'
import {sliderItems} from "../data"
import { mobile } from "../responsive"

const Container = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    /* background-color: #FDC2B1; */
    position: relative;
    overflow: hidden; // hide the overflow horizontal 
    ${mobile({ display : "none" })}
    
`
const Wrapper = styled.div`
    height: 100%;
    display: flex;    /* make vertical to horizontal */
    transition : all 1.5s ease;
    transform: translateX(${props => props.sliderIndex * -100}vw);

    @keyframes transition {
        from {opacity: .4} 
        to {opacity: 1}
}
    
`

const Slide = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    background-color: #${props => props.bg}; // props ~~ link the variable value to the function 
`
const ImgContainer = styled.div`
    height: 100%;
    width: 100%;
    flex: 1;
`

const Image = styled.img`

    width: 100%;
    height: 100%;
    object-fit: cover;
    
`


const InfoContainer = styled.div`
    flex: 1;
    padding: 50px;
`

const Title = styled.h1`
    font-family: 'Cinzel', serif;
    font-size: 70px;
`

const Description = styled.p`
    margin: 50px 0px;
    font-size: 20px;
    font-weight: 500;
    letter-spacing: 3px;
`

const Button = styled.button`
    padding: 10px;
    font-size: 20px;
    background-color: transparent;
    cursor: pointer;
`



const Arrow = styled.div`
    width: 50px;
    height: 50px;
    background-color: #fff7f7;
    border-radius: 50%;
    display: flex; 
    align-items: center; //  y axiz
    justify-content: center; // x axis
    position: absolute;
    top: 0;
    bottom: 0;
    left: ${props => props.direction === "left" && "10px"};
    right: ${props => props.direction === "right" && "10px"};
    cursor: pointer;
        // will use props  
        /* 
            arrow direction left and right sliders(?)
        
        */
    margin: auto;
    opacity: 0.5;
    z-index: 2;
`

const Slider = () => {



    /* 
        add handleClick function to add arrow functionality 

    */

    const [ sliderIndex, setSliderIndex ] = useState(0)
    const handleClick = (direction) =>  {
        if(direction === 'left'){
            setSliderIndex(sliderIndex > 0 ? sliderIndex-1 : 2 )
        } else {
            setSliderIndex(sliderIndex < 2 ? sliderIndex +1 : 0)
        }
    }

  return (
    <Container>
        {/* 
        
            Add arrow functionality
                ~ OnClick p
        
        */}
        <Arrow direction='left' onClick={() => handleClick("left")}> 
            <ArrowLeftOutlined/>
        </Arrow>

        <Wrapper sliderIndex = {sliderIndex}>
            {
            // SliderItems Hook (dont forget aaA)
            // sliderItems.map((item) => ()
            }
            {sliderItems.map((item) => (
                    <Slide bg={item.bg} key={item.id}>
                    <ImgContainer>
                        <Image src={item.img} />
                    </ImgContainer>
                    <InfoContainer>
                        <Title>
                            {item.title}
                        </Title>
                        <Description>
                            {item.desc}
                        </Description>

                        <Button>Lorem, ipsum.</Button>
                    </InfoContainer>
                    </Slide>
            ))}
         
        </Wrapper>
                
        <Arrow direction='right' onClick={() => handleClick("right")}  >
            <ArrowRightOutlined/> 
        </Arrow>
        
    </Container>
  )
}

export default Slider