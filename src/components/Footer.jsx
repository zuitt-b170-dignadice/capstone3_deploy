import { Facebook, Instagram, MailOutline, Phone, RoomOutlined, Twitter } from "@material-ui/icons"
import styled from "styled-components"
import { mobile } from "../responsive"

const Container = styled.div`
    display: flex;
    background-color: A5BECC;
    ${mobile({ flexDirection : "column" })}
`
const Left = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    padding: 20px;
`

const Logo = styled.h1``

const Desc = styled.p`
    margin: 20px 0px;
`

const SocialContainer = styled.div`
    display: flex;
`

const SocialIcon = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    color: black;
    background-color: #${props => props.color };
    display: flex;
    justify-content: center;;
    margin-right: 20px;
`

const Center = styled.div`
    flex: 1;
    padding: 20px;
    ${mobile({ display : "none" })}
`

const Title = styled.h3`
    margin-bottom: 30px;
`


const List = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
    display: flex;
    flex-wrap: wrap;
    
`

const ListItem = styled.li`
    width: 50%;
    margin-bottom: 10px;
    
`

const Right = styled.div`
    flex: 1;
    padding: 20px;
    ${mobile({ backgroundColor: "#c4bcbced" })}
`

const ContactItem = styled.div`
    margin-bottom: 20px;
    display: flex;
    align-items: center;
`

const Payment = styled.img`
    width: 100%;
`


const Footer = () => {
  return (
    <Container>
        <Left>
            <Logo>D & B Music Exchange</Logo>
            <Desc>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam doloribus hic cumque quaerat!</Desc>
            <SocialContainer>
                <SocialIcon color="#E9DFD1">
                    <Facebook />
                </SocialIcon>
                <SocialIcon color="#FCFDF4">
                    <Instagram />
                </SocialIcon>
            </SocialContainer>

        </Left>
        <Center>

            <Title>Links</Title>
            <List>
                <ListItem>Lorem.</ListItem>
                <ListItem>Lorem.</ListItem>
                <ListItem>Lorem.</ListItem>
                <ListItem>Lorem.</ListItem>
                <ListItem>Lorem.</ListItem>
                <ListItem>Lorem.</ListItem>
                <ListItem>Lorem.</ListItem>
            </List>
            
        </Center>
        <Right>
            <Title>Contact</Title>
            <ContactItem>
                <RoomOutlined style={{marginRight:"10px"}}  />
                Hiraya Building, Dasmarinas City, Cavite
            </ContactItem>
            <ContactItem>
                <Phone style={{marginRight:"10px"}} />
                +63 939 503 2932
            </ContactItem>
            <ContactItem>
                <MailOutline style={{marginRight:"10px"}} />
                dbmusicexch@gmai.com
            </ContactItem>
{/*             <Payment src="https://picsum.photos/40/20" /> */}
        </Right>
    </Container>
  )
}

export default Footer