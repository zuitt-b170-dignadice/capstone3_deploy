import React from 'react'
import styled from 'styled-components';
import { Search } from '@material-ui/icons'
import { Badge } from '@material-ui/core';
import { useSelector } from "react-redux"
import { Link } from "react-router-dom"


// icons
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';


// styled components inlay css under jsx

import { mobile } from '../responsive'

const Container = styled.div`
    top: 0;
    height: 60px;
    background-color: #7C3E66;
    background-image: linear-gradient(to right, #A5BECC , #7C3E66);
    color: #F2EBE9;
    position: sticky;
    z-index: 20;
    ${mobile({ height : "50px" })} // responsive 
`;

/* 
styled components 

just like CSS but in react.
*/

const Wrapper = styled.div`
   padding: 10px 20px;
   display: flex;
   align-items: center;
   justify-content: space-between;
   ${mobile({ padding : "10px 0px" })}
`;

const Left = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
`;

const Language = styled.span`
    font-size: 14px;
    cursor: pointer;
    ${mobile({ display : "none" })}
`

const SearchContainer = styled.div`
    /* border: 1px solid lightgray; */
    display: flex;
    align-items: center;
    margin-left: 25px;
    padding: 5px;
    
`
const Input = styled.input`
    border: none;
    ${mobile({ width : "50px" })}
`

const Center = styled.div`
    flex: 1;
    text-align: center;
`;

const Logo = styled.h1`
    font-family: 'Poppins', sans-serif;
    
    letter-spacing: 0.2px;
    ${mobile({ fontSize : "24px" })}
   
`


const Right = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    ${mobile({ flex: 2, justifyContent : "center" })}
`;

const MenuItem = styled.div`
    font-size: 14px;
    color: #F2EBE9;
    cursor: pointer;
    margin-left: 25px;
    text-decoration: none;
    ${mobile({ fontSize : "12px", marginLeft: "10px"  })}
`




const Navbar = () => {

  const quantity = useSelector(state => state.cart.quantity) // 0
  
  return (
    <Container>
        <Wrapper>
            <Left>
                <Language>EN</Language>
                <SearchContainer>
                    <Input placeholder="Search" />
                    <Search style={{ color : "#ee8568", fontSize:16, margin: "7px"}} />
                </SearchContainer>
            </Left>
            
            <Center>
                <Link to="/" style={{ textDecoration: "none", color: "#F2EBE9"}}>
                    <Logo >
                        D & B 
                    </Logo>
                </Link>
            </Center>
            <Right>
                <Link to="/register" style={{ textDecoration: "none", color: "#F2EBE9"}}>
                    <MenuItem>REGISTER</MenuItem>
                </Link>
                
                <Link to="/login" style={{ textDecoration: "none", color: "#F2EBE9"}}>
                    <MenuItem>LOG IN</MenuItem>
                </Link>
                <Link to="/cart">
                <MenuItem>
                    <Badge badgeContent={quantity} color="primary">
                        <ShoppingCartIcon />
                    </Badge>
                </MenuItem>
                </Link>
            </Right>
        </Wrapper>
    </Container>
  )
}

export default Navbar