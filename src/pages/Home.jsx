import React from 'react'
import Navbar from '../components/Navbar'
import Announcement from "../components/Announcement"
import Slider from '../components/Slider'
import Categories from '../components/Categories'
import Products from '../components/Products'
import Newsletter from '../components/Newsletter'
import Footer from '../components/Footer'


const Home = () => {
  return (
    <div>
        <Announcement />
        <Navbar />
        <Slider />
        <Categories />
        <Products />
        <Newsletter />
        <Footer />

    </div>
  )
}

export default Home

// rafce shortcut

/* import React from 'react'

const home = () => {
  return (
    <div>home</div>
  )
}

export default home */



// rfc

/* 
import React, { Component } from 'react'

export default class home extends Component {
  render() {
    return (
      <div>home</div>
    )
  }
}
 
*/