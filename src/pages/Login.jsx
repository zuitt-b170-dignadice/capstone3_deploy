import { useState } from "react"
import styled from "styled-components"
import { mobile } from "../responsive"
import { login } from "../redux/apiCalls"
import { useDispatch, useSelector } from "react-redux"
 

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background: linear-gradient(
        rgba(255,255,255,0.5),
        rgba(255,255,255,0.5)   
    ), 
    url(https://picsum.photos/id/500/1280/1280?grayscale) center;
    background-size: cover;
    display: flex;
    align-items: center;
    justify-content: center;

`

const Wrapper = styled.div`
    width: 25%;
    padding: 20px;
    background-color: white;
    ${mobile({ width : "75%" })}

`
const Title = styled.h1`
    font-family: 'Poppins', sans-serif;
    font-size: 24px;
    font-weight: 500;
    margin-bottom: 10px;
    text-align: center;
`

const Form = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`

const Input = styled.input`
    flex: 1;
    min-width: 40%;
    margin: 10px 0;
    padding: 10px;

`
const Button = styled.button`
    width: 40%;
    border: none;
    padding: 15px 20px;
    background-color: skyblue;
    color: white;
    cursor: pointer;
    align-items: center;
    justify-content: center;

    &:disabled{
        color: green;
        cursor: not-allowed;
    }
`
const Link = styled.a`
  margin: 5px 0px;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
  margin-bottom: 10px;
`

const Error = styled.span`
    color : red;
`

const Login = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const dispatch = useDispatch()
    const {isFetching, error} = useSelector((state) => state.user)

    const handleClick = (e) => {
        e.preventDefault()
        login(dispatch, {username, password})
    }
  return (
    <Container>
        <Wrapper>
            <Title>Log In</Title>
            <Form>
                <Input 
                placeholder="username" 
                onChange={(e) => setUsername(e.target.value)}
                />
                <Input 
                placeholder="password" 
                type="password"
                onChange={(e) => setPassword(e.target.value)}
                /> 
                <Button onClick={handleClick} disabled={isFetching}>
                    LOG IN
                </Button>
                {error && <Error>Something went wrong...</Error>}
                <Link>Forgot password?</Link>
                <Link>Create A new account</Link>
            </Form>
        </Wrapper>
    </Container>
  )
}

export default Login