
import Cart from "./pages/Cart";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Product from "./pages/Product";
import ProductList from "./pages/ProductList";
import Register from "./pages/Register";

// install react-router-dom@5.3.0 / I used legacy since im more comfortable of 
// import { Switch, Route } from "react-router-dom";

// 

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { useSelector } from "react-redux"
 
const App = () => {
  const user = useSelector((state) => state.user.currentUser)
  return (
    <Router>
      <Switch>
        <Route path="/home">
          <Home />
        </Route>
        <Route path="/products/:category">
          <ProductList />
        </Route>
        <Route path="/product/:id">
          <Product />
        </Route>
        <Route path="/cart">
          <Cart />
        </Route>
        <Route path="/login">
          {user ? <Redirect to="/" /> : <Login /> }
        </Route>
        <Route path="/register">
          {user ? <Redirect to="/" /> : <Register /> }
        </Route>
        <Redirect from="/" to="/home" />
      </Switch>
    </Router>
  )
};

export default App;