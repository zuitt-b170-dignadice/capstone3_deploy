export const sliderItems = [
    {
        id: 1,
        img: "https://www.leftoye.com/wp-content/uploads/2021/05/yvette-young-feature-image.jpg",
        title: "D & B Music Exchange",
        desc:  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porttitor nec magna et blandit. Quisque porttitor accumsan condimentum. Sed vulputate pulvinar.",
        bg : "A5BECC"
    },
    {
        id: 2,
        img: "https://media.npr.org/assets/img/2022/05/25/njf2021-cory-wong-2---photo-by-adam-kissick-0b37bc26a348d5b592101d374634ff6c0a92c8f7-s800-c85.webp",
        title: "GUITARS",
        desc:  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porttitor nec magna et blandit. Quisque porttitor accumsan condimentum. Sed vulputate pulvinar.",
        bg : "F2EBE9"
    },
    {
        id: 3,
        img: "https://upload.wikimedia.org/wikipedia/commons/4/4b/File-13-06-08_RaR_Pierce_the_Veil_Jaime_Preciado_01.jpg",
        title: "BASSES",
        desc:  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porttitor nec magna et blandit. Quisque porttitor accumsan condimentum. Sed vulputate pulvinar.",
        bg : "F2EBE9"
    }

]


export const categories = [
    {
        id: 1,
        img: "https://pbs.twimg.com/media/ByxnewQCcAAtu0R.jpg",
        title: "All Products",
        cat:"cat1"
    },
    {
        id: 2,
        img: "https://d2emr0qhzqfj88.cloudfront.net/s3fs-public/2019-01/Tim_Header01-mobile.jpg",
        title: "Guitars",
        cat: "guitar"
    },
    {
        id: 3,
        img: "https://pbs.twimg.com/media/EhCYq_cUwAE0FWX.jpg",
        title: "Basses",
        cat: "bass"
    }

]


export const popularProdcucts = [
    {
        id: 1,
        img: "https://picsum.photos/seed/picsum/500/500?grayscale",
    },
    {
        id: 2,
        img: "https://picsum.photos/seed/picsum/500/500?grayscale",
    },
    {
        id: 3,
        img: "https://picsum.photos/seed/picsum/500/500?grayscale",
    },
    {
        id: 4,
        img: "https://picsum.photos/seed/picsum/500/500?grayscale",
    },
    {
        id: 5,
        img: "https://picsum.photos/seed/picsum/500/500?grayscale",
    },
    {
        id: 6,
        img: "https://picsum.photos/seed/picsum/500/500?grayscale",
    },
    {
        id: 7,
        img: "https://picsum.photos/seed/picsum/500/500?grayscale",
    },
    {
        id: 8,
        img: "https://picsum.photos/seed/picsum/500/500?grayscale",
    }
]
